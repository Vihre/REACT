import { useFormik } from "formik";
import { Button } from "../button";
import { PatternFormat } from "react-number-format";
import { usersSchema } from "../../schemas";
import { useDispatch } from "react-redux";
import { sellBasketCards } from "../../redux/actions/basketCards";
import "./style.scss";

export function Form(props) {

	const { basketCards } = props;
	const dispatch = useDispatch();

	const formik = useFormik({
		initialValues: {
			firstName: "",
			lastName: "",
			age: "",
			adress: "",
			phoneNumber: "",
		},
		validationSchema: usersSchema,
		onSubmit: (values) => {

			console.log("Дані клієнта та адреса доставки:");
			console.log(`Ім'я: ${values.firstName}`);
			console.log(`Прізвище: ${values.lastName}`);
			console.log(`Вік: ${values.age}`);
			console.log(`Адреса: ${values.adress}`);
			console.log(`Номер телефону: ${values.phoneNumber}`);
			console.log("______________________________________________");
			console.log("Дані товару з кошика:");
			console.table(basketCards.map((card) => {
				return {
					Артикул: card.article,
					Назва: card.name,
					Ціна: card.price,
					Колір: card.color
				};
			}));

			const totalCost = basketCards.reduce((total, card) => {
				return total + card.price;
			}, 0);

			console.log(`Загальна вартість замовлення: ${totalCost} грн.`);
			console.log("______________________________________________");
			dispatch(sellBasketCards([]));

			formik.resetForm();
		},
	})

	return (
		<form className="form" onSubmit={formik.handleSubmit}>
			<label htmlFor="">
				<input
					type="text"
					name="firstName"
					id="firstName"
					placeholder="Ім'я"
					value={formik.values.firstName}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
				/>
			</label>
			{formik.errors.firstName && formik.touched.firstName ? (
				<div className="form__error">{formik.errors.firstName}</div>) : <div className="form__error"></div>}
			<label htmlFor="">
				<input
					type="text"
					name="lastName"
					id="lastName"
					placeholder="Призвіще"
					value={formik.values.lastName}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
				/>
			</label>
			{formik.errors.lastName && formik.touched.lastName ? (
				<div className="form__error">{formik.errors.lastName}</div>) : <div className="form__error"></div>}
			<label htmlFor="">
				<input
					type="number"
					name="age"
					id="age"
					placeholder="Вік"
					value={formik.values.age}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
				/>
			</label>
			{formik.errors.age && formik.touched.age ? (
				<div className="form__error">{formik.errors.age}</div>) : <div className="form__error"></div>}
			<label htmlFor="">
				<textarea
					name="adress"
					id="adress"
					cols="30"
					rows="10"
					placeholder="Адреса"
					value={formik.values.adress}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
				></textarea>
			</label>
			{formik.errors.adress && formik.touched.adress ? (
				<div className="form__error">{formik.errors.adress}</div>) : <div className="form__error"></div>}
			<label htmlFor="">
				<PatternFormat
					format="(###)###-##-##"
					mask="_"
					placeholder="Номер телефону"
					name="phoneNumber"
					id="phoneNumber"
					value={formik.values.phoneNumber}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
				/>
			</label>
			{formik.errors.phoneNumber && formik.touched.phoneNumber ? (
				<div className="form__error">{formik.errors.phoneNumber}</div>) : <div className="form__error"></div>}
			<Button type={'submit'} className={'btn-form'} text={'Оформити замовлення'} />
		</form>
	)
}