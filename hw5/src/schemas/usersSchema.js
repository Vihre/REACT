import * as Yup from "yup";

export const usersSchema = Yup.object({
	firstName: Yup.string()
		.matches(/^[a-zA-Zа-яА-Я]+$/, 'Поле має містити тільки літери')
		.min(2, "Мінімальна кількість літер 2")
		.max(20, "Максимальна кількість літер 20")
		.required("Поле обов'язкове для заповнення"),
	lastName: Yup.string()
		.matches(/^[a-zA-Zа-яА-Я]+$/, 'Поле має містити тільки літери')
		.min(2, "Мінімальна кількість літер 2")
		.max(20, "Максимальна кількість літер 20")
		.required("Поле обов'язкове для заповнення"),
	age: Yup.number()
		.min(18, 'Мінімальний вік - 18 років')
		.max(99, 'Максимальний вік - 99 років')
		.required("Поле обов'язкове для заповнення"),
	adress: Yup.string()
		.required("Поле обов'язкове для заповнення"),
	phoneNumber: Yup.string()
		.matches(/^\(\d{3}\)\d{3}-\d{2}-\d{2}$/, 'Невірний формат номеру телефона')
		.required("Поле обов'язкове для заповнення"),
});


