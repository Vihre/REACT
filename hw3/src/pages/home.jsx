import { ProductList } from "../components/product-list";

export function Home(props) {

	const { data, loading, error, toggleModal, addToBasket,
		addToFavorite, isModal, favsCards, basketCards, selectedCard, } = props

	const errorMessage = error ? <h2>An error has occurred</h2> : null;
	return (
		<>
			<h1 style={{ textAlign: 'center' }}>Мобільні тлефони</h1>
			<div className="cards__body">
				{loading ? (
					<div>loading data ...</div>
				) : (
					<>
						{errorMessage}
						<ProductList data={data}
							toggleModal={toggleModal}
							addToBasket={addToBasket}
							addToFavorite={addToFavorite}
							isModal={isModal}
							favsCards={favsCards}
							basketCards={basketCards}
							selectedCard={selectedCard}
						/>
					</>
				)}
			</div>
		</>
	)
}