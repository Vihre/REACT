import { basketCardsTypes } from "../types";
import { getDataFromLS } from "../../helper";

const initialState = {
	basketCards: getDataFromLS("cardsBasket"),
}

export function basketCardsReducer(state = initialState, action) {
	switch (action.type) {
		case basketCardsTypes.SET_BASKET_CARDS:
			return {
				...state,
				basketCards: action.payload,
			}
		default:
			return state
	}
}