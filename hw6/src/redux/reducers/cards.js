import { cardsTypes } from "../types";

const initialState = {
	loading: false,
	data: null,
	error: null,
	isLoaded: false
};

export function cardsReducer(state = initialState, action) {
	switch (action.type) {
		case cardsTypes.FETCH_DATA_START:
			return {
				...state,
				loading: true,
			}
		case cardsTypes.FETCH_DATA_SUCCESS:
			return {
				...state,
				data: action.payload,
				loading: false,
				error: null,
				isLoaded: true
			}
		case cardsTypes.FETCH_DATA_ERROR:
			return {
				...state,
				data: null,
				error: action.payload,
				loading: false,
			}
		default:
			return state
	}
}