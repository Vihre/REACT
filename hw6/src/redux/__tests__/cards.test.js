import { fetchDataStart, fetchDataSuccess, fetchDataError, getCardsAsync } from "../actions/cards";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

describe("cards", () => {
	describe("fetchDataStart", () => {
	  test("returns the correct action object", () => {
		 const expectedAction = {
			type: "FETCH_DATA_START"
		 };
 
		 expect(fetchDataStart()).toEqual(expectedAction);
	  });
	});

	describe("fetchDataSuccess", () => {
		test("returns the correct action object with data payload", () => {
		  const data = { /* your data object */ };
		  const expectedAction = {
			 type: "FETCH_DATA_SUCCESS",
			 payload: data
		  };
  
		  expect(fetchDataSuccess(data)).toEqual(expectedAction);
		});
	 });

	 
	 describe("fetchDataError", () => {
		test("returns the correct action object with error payload", () => {
		  const error = "An error occurred";
		  const expectedAction = {
			 type: "FETCH_DATA_ERROR",
			 payload: error
		  };
  
		  expect(fetchDataError(error)).toEqual(expectedAction);
		});
	 });
  
	 describe("getCardsAsync", () => {
		test("dispatches the correct actions", async () => {
		  const dispatchMock = jest.fn();
  
		  global.fetch = jest.fn().mockResolvedValueOnce({
			 json: jest.fn().mockResolvedValueOnce({ })
		  });
  
		  await getCardsAsync()(dispatchMock);
  
		  expect(dispatchMock).toHaveBeenNthCalledWith(1, fetchDataStart());
		  expect(dispatchMock).toHaveBeenNthCalledWith(2, fetchDataSuccess({  }));
		});
  
		test("dispatches the error action when fetch fails", async () => {
		  const dispatchMock = jest.fn();
  
		  global.fetch = jest.fn().mockRejectedValueOnce("Fetch error");
  
		  await getCardsAsync()(dispatchMock);
  
		  expect(dispatchMock).toHaveBeenNthCalledWith(1, fetchDataStart());
		  expect(dispatchMock).toHaveBeenNthCalledWith(2, fetchDataError("Fetch error"));
		});
	 });
});
